package com.xebia

import org.scalatest._

trait UnitSpecLike extends WordSpecLike with DiagrammedAssertions with OptionValues with TryValues with Inside with Inspectors

abstract class UnitSpec extends UnitSpecLike
