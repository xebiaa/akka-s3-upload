package com.xebia.akka.s3

import org.scalatest.Matchers._

import akka.http.model.StatusCodes._
import akka.http.testkit._

import com.xebia._

class PingRoutesSpec extends UnitSpec with ScalatestRouteTest {

  implicit val subject = new PingRoutes with RoutesSpecSupport

  "The PingRoutes handler" should {
    "return pong with status OK" in {
      Get("/ping") ~> subject.pingRoutes ~> check {
        status should equal(OK)
        responseAs[String] should equal("pong")
      }
    }
  }
}
