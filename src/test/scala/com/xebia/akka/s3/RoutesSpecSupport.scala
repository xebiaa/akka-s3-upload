package com.xebia.akka.s3

import scala.concurrent._

import akka.actor._
import akka.stream._

trait RoutesSpecSupport extends RoutesSupport {
  override implicit val system: ActorSystem = ActorSystem()
  override implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  override implicit val materializer: FlowMaterializer = ActorFlowMaterializer()
}
