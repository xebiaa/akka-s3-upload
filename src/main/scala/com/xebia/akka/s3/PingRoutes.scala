package com.xebia.akka.s3

import akka.http.server.Directives._

trait PingRoutes extends RoutesSupport {

  val pingRoutes =
    path("ping") {
      get {
        complete("pong")
      }
    }
}
