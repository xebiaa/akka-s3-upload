package com.xebia.akka.s3

import scala.concurrent._

import akka.actor._
import akka.stream._

trait RoutesSupport {
  implicit val system: ActorSystem
  implicit val executionContext: ExecutionContextExecutor
  implicit val materializer: FlowMaterializer
}
