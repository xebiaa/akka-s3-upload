package com.xebia.akka.s3

import akka.actor._
import akka.http.model._

import com.typesafe.config._

import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.ceedubs.ficus.readers._

import AmazonS3Support.AmazonSettings

object Settings extends ExtensionKey[Settings]

class Settings(rootConfig: Config) extends Extension {
  def this(system: ExtendedActorSystem) = this(system.settings.config)

  private val config = rootConfig.getConfig("xebia")

  object Http {
    val Port = config.as[Int]("http.port")
  }

  implicit val uriConfigReader: ValueReader[Uri] = stringValueReader.map(str ⇒ Uri(str))

  val amazonSettings = config.as[AmazonSettings]("amazon")
}
