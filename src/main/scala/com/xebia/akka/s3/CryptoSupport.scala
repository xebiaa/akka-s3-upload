package com.xebia.akka.s3;

import java.nio.charset.StandardCharsets._
import java.util.Base64
import javax.crypto._
import javax.crypto.spec._

object CryptoSupport {
  trait Bytes {
    def bytes: Array[Byte]

    lazy val asBase64 = Base64.getEncoder.encode(bytes)
    lazy val asBase64String = new String(asBase64, UTF_8)
  }

  case class Hmac(bytes: Array[Byte]) extends Bytes {
    override def toString = s"Hmac(${asBase64String})"
  }

  case class Key(bytes: Array[Byte]) extends Bytes {
    override def toString = s"Key(${asBase64String})"
  }

  object Key {
    def apply(data: String): Key = Key(data.getBytes(UTF_8))
  }

  def calculateHmacSha1(key: Key, value: String): Hmac = calculateHmac(value, initHmacCalculator(key, "HmacSHA1"))

  private def calculateHmac(value: String, hmacCalculator: Mac): Hmac = calculateHmac(value.getBytes(UTF_8), hmacCalculator)
  private def calculateHmac(value: Array[Byte], hmacCalculator: Mac): Hmac = Hmac(hmacCalculator.doFinal(value))

  private def initHmacCalculator(key: Key, macAlgorithm: String): Mac = {
    val hmacCalculator = Mac.getInstance(macAlgorithm)

    hmacCalculator.init(new SecretKeySpec(key.bytes, macAlgorithm))
    hmacCalculator
  }
}
