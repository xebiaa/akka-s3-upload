package com.xebia.akka.s3

import akka.actor.ActorSystem
import akka.http.Http
import akka.http.model._
import akka.http.server.Directives._
import akka.http.server._
import akka.stream.ActorFlowMaterializer
import akka.stream.scaladsl._
import headers.CacheDirectives.`no-cache`
import headers._

trait AmazonRoutes extends AmazonS3Support with RoutesSupport {

  def settings: Settings
  def amazonSettings = settings.amazonSettings

  val amazonRoutes =
    path("upload") {
      handleS3Upload {
        case Some(HttpResponse(StatusCodes.OK, _, _, _)) ⇒ complete(StatusCodes.OK)
        case Some(r @ HttpResponse(_, _, entity, _))     ⇒ complete(r)
        case None                                        ⇒ complete(StatusCodes.BadRequest)
      }
    } ~
      pathPrefix("download") {
        handleS3Download
      }

  def handleS3Upload: Directive1[Option[HttpResponse]] = {
    (put & extractRequest).flatMap { request ⇒
      extractFilename.flatMap { filename ⇒
        request.entity match {
          case e: UniversalEntity ⇒
            val data = request.entity.dataBytes
            val contentType = request.entity.contentType()
            val s3Request = createUploadRequest(contentType, filename).
              withEntity(HttpEntity.Default(request.entity.contentType(), e.contentLength, data))

            //Client connection to S3
            val outgoing = Http().outgoingConnection(s3BucketHost)
            val response = Source(List(s3Request))
              .via(outgoing)
              .runWith(Sink.head)

            onSuccess(response).flatMap { r ⇒
              provide(Some(r))
            }

          case _ ⇒ provide(None)
        }
      }
    }
  }

  def handleS3Download: Route = {
    get {
      path(Segment) { filename ⇒
        val s3Request = createDownloadRequest(filename)

        //Client connection to S3
        val outgoing = Http().outgoingConnection(s3BucketHost)
        val response = Source(List(s3Request))
          .via(outgoing)
          .runWith(Sink.head)

        complete(response)
      }
    }
  }

  def createDownloadRequest(filename: String): HttpRequest = {
    val dateHeader = amazonDateHeader
    val urlPath = s"/${filename}"
    val authHeader = amazonAuthHeader("GET", urlPath, dateHeader, None)
    val headers = dateHeader :: authHeader :: Nil

    HttpRequest(HttpMethods.GET, Uri(urlPath), headers)
  }

  private def createUploadRequest(contentType: ContentType, filename: String) = {
    import java.net.URLEncoder

    val dateHeader = amazonDateHeader
    val amzFilenameHeader = RawHeader("x-amz-meta-filename", URLEncoder.encode(filename, "UTF-8"))
    val urlPath = s"""/${filename}"""
    val authHeader = amazonAuthHeader("PUT", urlPath, dateHeader, Some(contentType), List(amzFilenameHeader))

    val headers = `Cache-Control`(`no-cache`) :: dateHeader :: amzFilenameHeader :: authHeader :: Nil

    HttpRequest(HttpMethods.PUT, Uri(urlPath), headers)
  }

  private def extractFilename = headerValuePF(contentDispositionFilename)

  private def contentDispositionFilename: PartialFunction[HttpHeader, String] = {
    case `Content-Disposition`(_, parameters) if parameters.contains("filename") ⇒ parameters("filename")
  }
}
