package com.xebia.akka.s3

import akka.http.server.Directives._

trait Routes extends AmazonRoutes with PingRoutes with RoutesSupport {

  lazy val routes =
    pathPrefix("api") {
      pingRoutes ~ amazonRoutes
    }
}
