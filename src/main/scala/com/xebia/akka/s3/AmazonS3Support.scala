package com.xebia.akka.s3

import java.time._
import java.time.format.DateTimeFormatter
import java.util.Locale

import akka.http.model._
import headers.RawHeader

object AmazonS3Support {
  case class AmazonSettings(accessKey: String, secretKey: String, host: String, baseDownloadUri: Uri, bucket: String)
}

trait AmazonS3Support extends AmazonDateTimeSupport {
  import AmazonS3Support._
  import CryptoSupport._

  def amazonSettings: AmazonSettings

  def s3BucketHost = s"${amazonSettings.bucket}.${amazonSettings.host}"

  def amazonAuthHeader(method: String, resource: String, dateHeader: HttpHeader, contentType: Option[ContentType], amzHeaders: List[HttpHeader] = List.empty[HttpHeader], contentMd5: String = "") = {
    val contentTypeValue = contentType.map(_.toString).getOrElse("")
    val signature = signAwsRequest(method, dateHeader.value, contentTypeValue, amazonSettings.bucket, resource, amzHeaders, contentMd5)

    RawHeader("Authorization", s"AWS ${amazonSettings.accessKey}:$signature")
  }

  def amazonDateHeader = RawHeader("Date", dateTimeToAmazonString(nowUTC))

  /**
   * Amazon S3 uploads and downloads need to have an authorization header containing a signature based on a very specific formatting
   * of several of the headers of the HTTP request.
   * For the details of the string-to-sign that is constructed see http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html?r=1821
   */
  private def signAwsRequest(method: String, dateString: String, contentType: String, bucket: String, resource: String, amzHeaders: List[HttpHeader] = List.empty[HttpHeader], contentMd5: String): String = {
    val bucketResource = s"/$bucket$resource"

    val signableAmzHeaders = amzHeaders.sortBy(_.name).map { header ⇒
      s"""${header.name}:${header.value}"""
    }
    val signatureElements = method :: contentMd5 :: contentType :: dateString :: signableAmzHeaders ::: bucketResource :: Nil
    val stringToSign = signatureElements.mkString("\n")

    calculateHmacSha1(Key(amazonSettings.secretKey), stringToSign).asBase64String
  }
}

trait AmazonDateTimeSupport {
  private val amazonDateTimePattern = "EEE, d MMM yyyy HH:mm:ss Z"
  private val amazonDateTimeFormat = DateTimeFormatter.ofPattern(amazonDateTimePattern).withZone(ZoneOffset.UTC).withLocale(Locale.US)

  def nowUTC = ZonedDateTime.now(ZoneOffset.UTC)
  def dateTimeToAmazonString(dateTime: ZonedDateTime) = amazonDateTimeFormat.format(dateTime)
}
