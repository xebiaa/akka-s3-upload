package com.xebia.akka.s3

import akka.actor._
import akka.http.Http
import akka.stream._

object Main extends App with Routes {
  override implicit val system = ActorSystem("akka-s3-upload")
  override implicit val executionContext = system.dispatcher
  override implicit val materializer = ActorFlowMaterializer()

  override lazy val settings = Settings(system)

  Http(system).bindAndHandle(handler = routes, interface = "0.0.0.0", port = settings.Http.Port)
}
