# akka-s3-upload

### API

| API                            | METHODS |
|--------------------------------|---------|
| /api/ping                      | GET     |
| /api/upload                    | POST    |

