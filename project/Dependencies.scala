import sbt._
import sbt.Keys._

object Dependencies {

  lazy val dependencySettings = Seq(
    resolvers := Seq(
      "Sonatype Releases"   at "https://oss.sonatype.org/content/repositories/releases",
      "Sonatype Snapshots"  at "https://oss.sonatype.org/content/repositories/snapshots",
      "Spray Repository"    at "http://repo.spray.io/",
      "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
    )
  )

  val akkaVersion = "2.3.9"
  val akkaHttpVersion = "1.0-M5"

  // compile-time dependencies
  val akkaActor                = "com.typesafe.akka"       %% "akka-actor"                      % akkaVersion
  val akkaHttp                 = "com.typesafe.akka"       %% "akka-http-experimental"          % akkaHttpVersion
  val akkaSlf4j                = "com.typesafe.akka"       %% "akka-slf4j"                      % akkaVersion
  val ficus                    = "net.ceedubs"             %% "ficus"                           % "1.1.2"

  // runtime dependencies
  val logback                  = "ch.qos.logback"          %  "logback-classic"                 % "1.1.2"

  // test dependencies
  val akkaTestkit              = "com.typesafe.akka"       %% "akka-testkit"                    % akkaVersion
  val akkaHttpTestkit          = "com.typesafe.akka"       %% "akka-http-testkit-experimental"  % akkaHttpVersion
  val scalatest                = "org.scalatest"           %% "scalatest"                       % "2.2.4"

  def compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")
  def provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
  def container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")
}
