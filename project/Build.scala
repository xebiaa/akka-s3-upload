import sbt._
import sbt.Keys._

import spray.revolver.RevolverPlugin.Revolver

object Build extends Build {
  import Dependencies._
  import Formatting._

  lazy val basicSettings = Seq(
    organization := "com.xebia",
    version := "0.1.0",
    scalaVersion := "2.11.5",
    scalacOptions := fussyScalacOptions,
    incOptions := incOptions.value.withNameHashing(true),
    parallelExecution in Test := false
  )

  lazy val libSettings = basicSettings ++ dependencySettings ++ formattingSettings
  lazy val appSettings = libSettings ++ Revolver.settings

  lazy val rebbCommon = Project("akka-s3-upload", file("."))
    .settings(basicSettings: _*)
    .settings(appSettings: _*)
    .settings(mainClass := Some("com.xebia.akka.s3.Main"))
    .settings(libraryDependencies ++=
      compile(
        akkaActor,
        akkaSlf4j,
        akkaHttp,
        ficus
      ) ++
      runtime(
        logback
      ) ++
      test(
        akkaTestkit,
        akkaHttpTestkit,
        scalatest
      ))

  // ==========================================================================
  // ScalacOptions
  // ==========================================================================

  val basicScalacOptions = Seq(
    "-encoding", "utf8",
    "-target:jvm-1.7",
    "-feature",
    "-language:implicitConversions",
    "-language:postfixOps",
    "-unchecked",
    "-deprecation",
    "-Xlog-reflective-calls"
  )

  val fussyScalacOptions = basicScalacOptions ++ Seq(
    "-Ywarn-unused",
    "-Ywarn-unused-import"
  )
}
